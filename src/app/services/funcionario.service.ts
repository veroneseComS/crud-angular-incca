import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Funcionario } from 'src/models/funcionario.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FuncionarioService {

  constructor(public _http: HttpClient) { }

  cadastraFuncionario(funcionario: Funcionario, idEmpresa: number):Observable<any>{
    return this._http.post<any>('https://crudbackendincca20190613102520.azurewebsites.net/api/funcionario', 
    {
      empresaId: idEmpresa,
      nome: funcionario.nome,
      cargo: funcionario.cargo,
      salario: funcionario.salario
    })
  }

  listaFuncionarios():Observable<any>{
    return this._http.get<any>('https://crudbackendincca20190613102520.azurewebsites.net/api/funcionario')
  }

  salvaAlteracoesFuncionario(funcionario: Funcionario):Observable<any>{
    return this._http.put<any>('https://crudbackendincca20190613102520.azurewebsites.net/api/funcionario/'+funcionario.funcionarioId, {
      funcionarioId: funcionario.funcionarioId,
      nome: funcionario.nome,
      salario: funcionario.salario,
      cargo: funcionario.cargo,
      empresaId: funcionario.empresaId
    })
  }

  excluiFuncionario(idFunc):Observable<any>{
    return this._http.delete<any>('https://crudbackendincca20190613102520.azurewebsites.net/api/funcionario/'+idFunc)
  }

}