import { Injectable } from '@angular/core';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { Observable } from 'rxjs';
import { Empresa } from 'src/models/empresa.model';
import { Funcionario } from 'src/models/funcionario.model';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {

  constructor(private _http: HttpClient) { }

  listaEmpresas():Observable<any>{
    return this._http.get<any>('https://crudbackendincca20190613102520.azurewebsites.net/api/empresa')
  }

  cadastraEmpresa(empresa: Empresa):Observable<any>{
    return this._http.post<any>('https://crudbackendincca20190613102520.azurewebsites.net/api/empresa', 
    {
      nome: empresa.nome,
      cep: empresa.cep,
      logradouro: empresa.logradouro,
      complemento: empresa.complemento,
      numero: empresa.numero,
      bairro: empresa.bairro,
      localidade: empresa.localidade,
      uf: empresa.uf,
      unidade: empresa.unidade,
      ibge: empresa.ibge,
      gia: empresa.gia,
      telefone: empresa.telefone
    })
  }

  excluiEmpresa(idEmpresa):Observable<any>{
    return this._http.delete<any>('https://crudbackendincca20190613102520.azurewebsites.net/api/empresa/'+idEmpresa)
  }

  alteraEmpresa(empresa: Empresa):Observable<any>{
    return this._http.put<any>('https://crudbackendincca20190613102520.azurewebsites.net/api/empresa/'+empresa.empresaId, 
    {
      empresaId: empresa.empresaId,
      nome: empresa.nome,
      cep: empresa.cep,
      logradouro: empresa.logradouro,
      complemento: empresa.complemento,
      numero: empresa.numero,
      bairro: empresa.bairro,
      localidade: empresa.localidade,
      uf: empresa.uf,
      unidade: empresa.unidade,
      ibge: empresa.ibge,
      gia: empresa.gia,
      telefone: empresa.telefone
    })
  }

}