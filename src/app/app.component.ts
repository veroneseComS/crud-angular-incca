import { FuncionarioService } from './services/funcionario.service';
import { EmpresaService } from './services/empresa.service';
import { CepService } from './services/cep-service.service';
import { Component } from '@angular/core';
import { Empresa } from 'src/models/empresa.model';
import { Funcionario } from 'src/models/funcionario.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  
  mostraCrudFuncionario: boolean = false;
  mostraDetalhesEmpresa = [];
  cadastro: boolean = true;
  title = 'CrudInccaFrontEnd';
  empresa: Empresa = new Empresa();
  funcionario: Funcionario = new Funcionario();
  funcionarios;
  empresas: Empresa[];
  loading: boolean = false;
  hideme=[]
  mostraMensagemErro: string;
  hidenfuncionario=[]
  hidemefuncionariolista=[]
  listafuncionarios: Funcionario[]

  constructor(public cepService: CepService,
              public empresaService: EmpresaService,
              public funcionarioService: FuncionarioService
              ){
    this.empresa.cep = ""
    this.empresa.telefone = ""
  }

  ngOnInit() {
    this.listaEmpresasCadastradas();
    this.listaTodosFuncionarios();
  }


  MostraEscondeDadosEmpresa(idEmpresa){
    console.log(idEmpresa)
    if(this.mostraDetalhesEmpresa.includes(idEmpresa)){
      for(let i=0;i<this.mostraDetalhesEmpresa.length;i++){
        if(this.mostraDetalhesEmpresa[i] == idEmpresa){
          this.mostraDetalhesEmpresa.splice(i,1)
        }
      }
    }else{
      this.mostraDetalhesEmpresa.push(idEmpresa)
    }
    console.log(this.mostraDetalhesEmpresa);
  }

  confirmaAcao(){
    if(this.cadastro){
      this.cadastraEmpresa()
    }else{
      this.salvaAlteracoesEmpresa()
    }
  }

  cancelaAcao(){
    this.cadastro = true;
    this.empresa = new Empresa();
  }

  cadastraEmpresa(){
    this.empresaService.cadastraEmpresa(this.empresa)
    .subscribe((res) => {
      this.empresas.push(res)
      this.empresa = new Empresa();
    }, 
    (err) => {
      console.log(err)
    })
  }

  salvaAlteracoesEmpresa(){
    this.empresaService.alteraEmpresa(this.empresa)
    .subscribe((res) => {
      this.empresa = new Empresa();
      this.cadastro = true;
      this.listaEmpresasCadastradas()
    })
  }

  listaEmpresasCadastradas(){
    this.empresaService.listaEmpresas()
    .subscribe((res) => {
      this.empresas = res;
    }, 
    (err) => {
      console.log(err)
    })
  }

  editaEmpresa(empresa){
    this.cadastro = false;
    this.empresa = JSON.parse(JSON.stringify(empresa));
    window.scroll(0,0);
  }

  editaFuncionario(funcionario,indexfuncionario){
    this.funcionario = JSON.parse(JSON.stringify(funcionario));
    this.mostraCrudFuncionario = true;
  }

  cancelaAcaoFuncionario(index?){
    this.mostraCrudFuncionario = false;
    this.funcionario = new Funcionario();
    this.hidenfuncionario[index] = false;
  }

  cadastraAlteraFuncionario(idEmpresa, index){
    if(this.mostraCrudFuncionario == false){
      this.cadastraFuncionario(idEmpresa, index)
    }else{
      this.salvaAlteracoesFuncionario(idEmpresa)
    }
  }

  salvaAlteracoesFuncionario(idEmpresa){
    this.funcionario.empresaId = idEmpresa
    this.funcionarioService.salvaAlteracoesFuncionario(this.funcionario)
    .subscribe((res) => {
      this.mostraCrudFuncionario = false;
      this.listaTodosFuncionarios();
      this.listaEmpresasCadastradas();
    }, 
    (err) => {
      console.log(err)
    })
  }

  excluiEmpresa(idEmpresa){
    this.empresaService.excluiEmpresa(idEmpresa)
    .subscribe((res) => {
      this.listaEmpresasCadastradas();
      this.listaTodosFuncionarios();
    },
    (err) => {
      console.log(err)
    })
  }

  consultaDadosCep(){
    if(this.empresa.cep.length > 7){
      this.cepService.listaDadosCep(this.empresa.cep)
      .subscribe((res) => {
        this.empresa.logradouro = res.logradouro
        this.empresa.complemento = res.complemento;
        this.empresa.bairro = res.bairro;
        this.empresa.localidade = res.localidade;
        this.empresa.uf = res.uf;
        this.empresa.unidade = res.unidade;
        this.empresa.ibge = res.ibge;
        this.empresa.gia = res.gia;
        this.empresa.telefone = res.telefone;
      }, 
      (err) => {
        console.log(err)
      })
    }
  }

  cadastraFuncionario(idEmpresa: number, indexEmpresa: number){
    this.funcionarioService.cadastraFuncionario(this.funcionario, idEmpresa)
    .subscribe((res) => {
      this.empresas[indexEmpresa].funcionarios.push(res)
      this.funcionario = new Funcionario();
      this.mostraMensagemErro = ''
    }, 
    (err) => {
      console.log('entrou err');
      this.mostraMensagemErro = 'Não é possível adicionar um usuário já vinculado a uma empresa.'
    })
  }

  listaTodosFuncionarios(){
    this.funcionarioService.listaFuncionarios()
    .subscribe((res) => {
      console.log(res);
      this.listafuncionarios = res;
    }, 
    (err) => {
      console.log(err)
    })
  }

  excluiFuncionario(idFuncionario: number, indexEmpresa, indexFuncionario){
    this.funcionarioService.excluiFuncionario(idFuncionario)
    .subscribe((res) => {
      this.empresas[indexEmpresa].funcionarios.splice(indexFuncionario,1);
    }, 
    (err) => {
      console.log(err)
    })
  }

}