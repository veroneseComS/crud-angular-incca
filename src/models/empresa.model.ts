export class Empresa{
    empresaId: number
    nome: string
    cep: string
    logradouro: string
    complemento: string
    numero: number
    bairro: string
    localidade: string
    uf: string
    unidade: number
    ibge: number
    gia: number
    telefone: string
    funcionarios: [{
        cargo: string;
        funcionarioId: number;
        empresaId: number;
        nome: string;
        salario: string;
    }]
}